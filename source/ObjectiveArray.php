<?php

namespace Zalmoksis\Objectify;

class ObjectiveArray {
    final function __construct(
        protected array $array = []
    ) {}

    static function from(array $array = []): static {
        return new static($array);
    }

    function getArray(): array {
        return $this->array;
    }

    function chunk(int $length): ObjectiveArray {
        $chunks = [];

        foreach (array_chunk($this->array, $length, true) as $chunk) {
            $chunks[] = new static($chunk);
        }

        return new ObjectiveArray($chunks);
    }

    function flip(): ObjectiveArray {
        return new ObjectiveArray(array_flip($this->array));
    }

    function hasKey(string | int $key): bool {
        return array_key_exists($key, $this->array);
    }

    function implode(string $separator = ''): ObjectiveString {
        return new ObjectiveString(implode($separator, $this->array));
    }

    function isEmpty(): bool {
        return $this->array === [];
    }

    function map(callable $callback): static {
        return new static(array_map($callback, $this->array));
    }
}
