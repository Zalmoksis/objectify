<?php

namespace Zalmoksis\Objectify;

class ObjectiveString {
    final function __construct(
        protected string $string = ''
    ) {}

    static function from(string $string = ''): static {
        return new static($string);
    }

    function getString(): string {
        return $this->string;
    }

    function __toString(): string {
        return $this->string;
    }

    function contains(string $needle): bool {
        return str_contains($this->string, $needle);
    }

    function explode(string $separator): ObjectiveArray {
        return new ObjectiveArray(explode($separator, $this->string));
    }
}
