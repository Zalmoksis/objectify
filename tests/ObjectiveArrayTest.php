<?php

namespace Zalmoksis\Objectify\Tests;

use Zalmoksis\Objectify\ObjectiveArray;
use PHPUnit\Framework\TestCase;

class ObjectiveArrayTest extends TestCase {

    function testNamedConstructor(): void {
        $this->assertEquals(
            new ObjectiveArray(['a', 'b', 'c']),
            ObjectiveArray::from(['a', 'b', 'c'])
        );
    }

    function testGetArray(): void {
        $this->assertEquals([], (new ObjectiveArray())->getArray());
        $this->assertEquals(['a', 'b', 'c'], (new ObjectiveArray(['a', 'b', 'c']))->getArray());
    }

    function testChunk(): void {
        $this->assertEquals(
            new ObjectiveArray([
                new ObjectiveArray(['a', 'b']),
                new ObjectiveArray([2 => 'c'])
            ]),
            (new ObjectiveArray(['a', 'b', 'c']))->chunk(2)
        );
    }

    function testFlip(): void {
        $this->assertEquals(
            new ObjectiveArray(['x' => 'a', 'y' => 'b']),
            (new ObjectiveArray(['a' => 'x', 'b' => 'y']))->flip()
        );
    }

    function testHasKey(): void {
        $objectiveArray = new ObjectiveArray(['a' => 'x', 'b' => null]);
        $this->assertTrue($objectiveArray->hasKey('a'));
        $this->assertTrue($objectiveArray->hasKey('b'));
        $this->assertFalse($objectiveArray->hasKey('c'));
    }

    function testImplode(): void {
        $this->assertEquals(
            'a|b|c',
            (new ObjectiveArray(['a', 'b', 'c']))->implode('|')
        );
    }

    function testIsEmpty(): void {
        $this->assertTrue((new ObjectiveArray())->isEmpty());
        $this->assertFalse((new ObjectiveArray(['a', 'b', 'c']))->isEmpty());
    }

    function testMap(): void {
        $this->assertEquals(
            new ObjectiveArray(['A', 'B', 'C']),
            (new ObjectiveArray(['a', 'b', 'c']))->map('strtoupper')
        );
    }
}
