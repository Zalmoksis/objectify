<?php

namespace Zalmoksis\Objectify\Tests;

use Zalmoksis\Objectify\ObjectiveArray;
use Zalmoksis\Objectify\ObjectiveString;
use PHPUnit\Framework\TestCase;

class ObjectiveStringTest extends TestCase {

    function testNamedConstructor(): void {
        $this->assertEquals(
            new ObjectiveString('lamprey'),
            ObjectiveString::from('lamprey')
        );
    }

    function testGetString(): void {
        $this->assertEquals('', (new ObjectiveString())->getString());
        $this->assertEquals('lamprey', (new ObjectiveString('lamprey'))->getString());
    }

    function testCastingToString(): void {
        $this->assertEquals('', (string)(new ObjectiveString()));
        $this->assertEquals('lamprey', (string)(new ObjectiveString('lamprey')));
    }

    function testContains(): void {
        $this->assertTrue(
            (new ObjectiveString('The quick brown fox jumps over a lazy dog'))->contains('brow')
        );
    }

    function testExplode(): void {
        $this->assertEquals(
            new ObjectiveArray(['a', 'b', 'c']),
            (new ObjectiveString('a|b|c'))->explode('|')
        );
    }
}
