# Objectify

[![pipeline status](https://gitlab.com/Zalmoksis/objectify/badges/master/pipeline.svg)](https://gitlab.com/Zalmoksis/objectify/-/commits/master)
[![coverage report](https://gitlab.com/Zalmoksis/objectify/badges/master/coverage.svg)](https://gitlab.com/Zalmoksis/objectify/-/commits/master)

Objectified string and array for PHP
